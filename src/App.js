import {BrowserRouter as Router,} from "react-router-dom";
import SideMenu from './components/sideMenu'
import Main from './components/main'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';


function App() {

  return (
    <div className="app" id="outer-container">
      <Router>
        <SideMenu/>
        <Main/>
      </Router>
    </div>
  );
}

export default App;
