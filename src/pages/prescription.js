import React, {useState} from 'react'
function Prescription() {

    const [datos, setDatos] = useState({
        doctor: "",
        paciente: "",
        enfermedad: "",
        hospital: "",
        entrada: "",
        salida: "",
        costo: "",
        notas: ""
    })

    const handleInputChange = (event) => {
        setDatos({
            ...datos,
            [event.target.name] : event.target.value
        })
      }

    const handleForm = (event) => {
        event.preventDefault();
        setDatos({
            doctor: "",
            paciente: "",
            enfermedad: "",
            hospital: "",
            entrada: "",
            salida: "",
            costo: "",
            notas: ""
        })
        alert("consulta guardada")
    }

    return(

            <div class="row">
                <div class="col-md-2">
                </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body"></div>
                            <form onSubmit={handleForm}>
                                <h1> Prescripcion </h1>
                                <label>Doctor</label>
                                <select class="form-control" name="doctor" onChange={handleInputChange} value={datos.doctor}>
                                    <option value="0"> Seleccionar Doctor
                                    </option>
                                    <option value="1"> Dr. Joaquin Ochoa
                                    </option>
                                    <option value="2"> Dra. Lucrecia Garcia
                                    </option>
                                </select>
                                <br></br>
                                <label>Paciente</label>
                                <select class="form-control" name="paciente" onChange={handleInputChange} value={datos.paciente}>
                                    <option value="0"> Seleccionar Paciente
                                    </option>
                                    <option value="1"> Fernando Rosales
                                    </option>
                                    <option value="2"> Brenda Ramirez
                                    </option>
                                </select>
                                <br></br>
                                <label>Enfermedad</label>
                                <select class="form-control" name="enfermedad" onChange={handleInputChange} value={datos.enfermedad}>
                                    <option value="0"> Seleccionar Enfermedad
                                    </option>
                                    <option value="1"> Hepatitis B
                                    </option>
                                    <option value="2"> Gastritis
                                    </option>
                                </select>
                                <br></br>
                                <label>Hospital</label>
                                <select class="form-control" name="hospital" onChange={handleInputChange} value={datos.hospital}>
                                    <option value="0"> Seleccionar Hospital
                                    </option>
                                    <option value="1"> Blue Medicar 
                                    </option>
                                    <option value="2"> IGSS
                                    </option>
                                </select>
                                <br></br>
                                <label>Hora de entrada</label>
                                <br></br>
                                <input type="datetime-local" name="entrada" onChange={handleInputChange} value={datos.entrada} class="form-control"></input>
                                <br></br>
                                <label>Hora de salida</label>
                                <br></br>
                                <input type="datetime-local" name="salida" onChange={handleInputChange} value={datos.salida} class="form-control"></input>
                                <br></br>
                                <br></br>
                                <label>Costo de Prescripcion</label>
                                <br></br>
                                <input type="number" name="costo" onChange={handleInputChange} value={datos.costo} class="form-control"></input>
                                <br></br>
                                <label>Notas</label>
                                <br></br>
                                <textarea class="form-control" name="notas" onChange={handleInputChange} value={datos.notas} rows="3"></textarea>
                                <br></br>
                                <button class="btn btn-secondary">Save</button>
                            </form>
                            
                        </div>
                </div>
            </div>
        

    );
}
export default Prescription;