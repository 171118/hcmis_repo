function Home() {
    console.log("home")
    return(
        <div>
            <div className="jumbotron">
                <h1> Bienvenidos a HCMIS </h1>
            </div>
            <div>
                <h3>Centro de ayuda</h3>
                <hr></hr>
                <p>Si se llega a presentar un problema 
                    con la aplicación por favor comunicarse al correo:  jira@hcmis.atlassian.net, si el problema es muy grave.
                    Si el problema que presenta en la aplicacion es algo simple en la esquina inferior derecha se encuentra 
                    un icono de interrogacion para comunicar el problema 
                    que esta ocurriendo con la aplicación</p>
            </div>
        </div>

    );
}
export default Home;