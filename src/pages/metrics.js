import { VictoryChart, VictoryBar, VictoryPie, VictoryLine } from 'victory';

function Metrics() {

    const data = [
        {x: "Gripe", y: 3000},
        {x: "Hepatitis", y: 250},
        {x: "Gastritis", y: 800},
        {x: "Intoxicacion", y: 300}
      ];

    const total = data.map(x => x.y).reduce((a, b) => a + b, 0);

    const data2 = [
      {x: "01/1/2021", y: 500},
      {x: "01/2/2021", y: 300},
      {x: "01/3/2021", y: 800},
      {x: "01/4/2021", y: 1000}
    ];

    return(
        <div>
          <h1>Metrics</h1>
          <hr/>
            

          <div >
            <div className="row">
              <div className="col-sm">
                <h4>Enfermedades</h4>
                  <VictoryChart domainPadding={20} padding={{ top: 2, bottom: 60, left: 60, right: 60 }} height={300}>
                    <VictoryBar
                      style={{ data: { fill: "#000000" } }}
                      data={data}
                      animate={{
                        duration: 2000,
                        onLoad: { duration: 1000 }
                      }}
                    />
                  </VictoryChart>
              </div>

              <div className="col-sm">
              <h4>Pacientes</h4>
                    <VictoryPie
                      height={300}
                      padding={{ top: 60, bottom: 60, left: 120, right: 100 }}
                      colorScale={["tomato", "orange", "cyan", "navy" ]}
                      labels={({ datum }) => `${datum.x}: ${((datum.y / total) * 100 ).toFixed(2)}%`}
                      innerRadius={40}
                      animate={{
                        duration: 2000,
                        onLoad: { duration: 1000 }, onEnter: {duration: 1000}
                      }}
                      data={data}
                    />
              </div>

              <div className="col-sm">
              <h4>Recuperación de pacientes</h4>
                  <VictoryChart domainPadding={20} padding={{ top: 2, bottom: 60, left: 60, right: 60 }} height={300}>
                    <VictoryLine
                      data={data2}
                      animate={{
                        duration: 2000,
                        onLoad: { duration: 1000 }
                      }}
                    />
                  </VictoryChart>
              </div>
              
            </div>
          </div>
        </div>

    );
}
export default Metrics;