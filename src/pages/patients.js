import React, {useState} from 'react';
import { FaSearch, FaNotesMedical } from 'react-icons/fa'
import { Spinner } from 'react-bootstrap';

const datos = [
    {id: 1, nombre: "Nelson", edad: '19', correo: 'Nelson@gmail.com', telefono: '177013'}, 
    {id: 2, nombre: "Horacio", edad: '30', correo: 'Horacio@gmail.com', telefono: '285504'}, 
    {id: 3, nombre: "Elena", edad: '40', correo: 'Elena@gmail.com', telefono: '310841'}, 
    {id: 4, nombre: "Nicolás", edad: '25', correo: 'Nicolas@gmail.com', telefono: '242069'}, 
    {id: 5, nombre: "Teresa", edad: '30', correo: 'Teresa@gmail.com', telefono: '153006'},
    {id: 6, nombre: "Ana", edad: '30', correo: 'Ana@gmail.com', telefono: '340551'},
    {id: 7, nombre: "Isaac", edad: '30', correo: 'Isaac@gmail.com', telefono: '340555'},
]

const  highlightText = (text, query) => {
    let lastIndex = 0;
    const words = query
        .split(/\s+/)
        .filter(word => word.length > 0);
    if (words.length === 0) {
        return [text];
    }
    const regexp = new RegExp(words.join("|"), "gi");
    const tokens = [];
    while (true) {
        const match = regexp.exec(text);
        if (!match) {
            break;
        }
        const length = match[0].length;
        const before = text.slice(lastIndex, regexp.lastIndex - length);
        if (before.length > 0) {
            tokens.push(before);
        }
        lastIndex = regexp.lastIndex;
        tokens.push(<strong key={lastIndex}>{match[0]}</strong>);
    }
    const rest = text.slice(lastIndex);
    if (rest.length > 0) {
        tokens.push(rest);
    }
    return tokens;
}

function Patients() {

    const [loading, setLoad] = useState(false);
    const [click, setClick] = useState(false);
    const [search, setSearch] = useState("");

    const handleSearch = (event) => {
        setSearch(event.target.value)
    }

    const handleClck = () => {
        setClick(true);
        //setLoad(true)
    }

    const items = datos.filter((data)=>{
        if(search === ""){
            return data}
        else{ if(data.id.toString().toLowerCase().includes(search.toLowerCase()) || 
        data.nombre.toLowerCase().includes(search.toLowerCase()) || 
        data.edad.toLowerCase().includes(search.toLowerCase()) ||
        data.correo.toLowerCase().includes(search.toLowerCase()) ||
        data.telefono.toLowerCase().includes(search.toLowerCase())){
            return data
        }}
      }).map((cli, index)=>{
        return(
          <tr key={index}>
            <th>{highlightText(cli.id.toString(), search)}</th>
            <td>{highlightText(cli.nombre, search)}</td>
            <td>{highlightText(cli.edad, search)}</td>
            <td>{highlightText(cli.correo, search)}</td>
            <td>{highlightText(cli.telefono, search)}</td>
          </tr>
        )
      })

      const table = (
        <section className="table-responsive container-fluid">
                <table className="table table-striped table-hover table-sm">
                    <thead className="thead-dark">
                    <tr>
                        <th scope="col" >IDd</th>
                        <th scope="col" >Nombre</th>
                        <th scope="col" >Edad</th>
                        <th scope="col" >Correo</th>
                        <th scope="col" >Telefono</th>
                    </tr>
                    </thead>
                    <tbody>
                    {items}
                    </tbody>
                </table>
                </section>
      )

      const spin = (
          <div>
              <center>
              <Spinner animation="border" />
              </center>
          </div>
      )

      const instr = (
        <div style={{color: 'gray'}}>
        <center>
            <h2>Buscar pacientes</h2>
        </center>
    </div>
      )

    return(
        <div>
            <h1>Pacientes</h1>
            <hr/>
            <div className="input-group">
                <div className="form-outline">
                    <input type="search" onChange={handleSearch} id="form1" placeholder="Search" className="form-control" />
                </div>
                <button type="button" className="btn btn-dark">
                    <FaSearch />
                </button>
                &nbsp;&nbsp;
                <button type="button" className="btn btn-dark" onClick={handleClck}>
                    <FaNotesMedical /> Mis Pacientes
                </button>
            </div>
            <br/>
            {loading ? spin : (click ? table : instr)}
            
            
        </div>
    )
}

export default Patients;