import React from "react";
import {
  Switch,
  Route,
} from "react-router-dom";

import Home from '../pages/home'
import Metrics from '../pages/metrics'
import Calendar from '../pages/calendar'
import Prescription from '../pages/prescription'
import Patients from '../pages/patients'
  
  function Main(){
  
    return(
      <main>
          <Switch>
          <Route exact={true} path="/">
            <Home />
          </Route>
          <Route exact={true} path="/calendar">
            <Calendar />
          </Route>
          <Route exact={true} path="/prescription">
            <Prescription />
          </Route>
          <Route exact={true} path="/metrics">
            <Metrics />
          </Route>
          <Route exact={true} path="/patients">
            <Patients />
          </Route>
        </Switch>
      </main>
    );
  }
  
  export default Main;