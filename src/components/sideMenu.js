import { ProSidebar,
  Menu,
  MenuItem,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,} from 'react-pro-sidebar';
import {Link} from "react-router-dom";
  import {useState} from 'react'
import { FaArrowCircleLeft, FaArrowCircleRight, FaPrescriptionBottleAlt, FaNotesMedical } from 'react-icons/fa';
import {BsHouseFill, BsCalendarFill} from 'react-icons/bs'
import { GiHospital } from 'react-icons/gi';
import { GoGraph } from 'react-icons/go'
import 'react-pro-sidebar/dist/css/styles.css';

function SideMenu(){

  const [collapsed, setCollapsed] = useState(false);

  const collaps = () =>{
    setCollapsed(collapsed ? false : true);
  }

  return(
    <ProSidebar collapsed={collapsed}>
      <SidebarHeader>
        <div
          style={{
            padding: '5px 10px',
            textTransform: 'uppercase',
            fontWeight: 'bold',
            fontSize: 60,
            letterSpacing: '1px',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            display: 'flex'
          }}
          >
            <GiHospital/>
            <div style={{fontSize : 30, paddingTop: '30px', display: collapsed ? 'none' : 'flex'}} >HCMIS</div>
          </div>
        </SidebarHeader>
        <SidebarContent>
          <Menu iconShape="circle">
            <MenuItem icon={<BsHouseFill />}>Home<Link to="/" /></MenuItem>
            <MenuItem icon={<BsCalendarFill />}>Calendario<Link to="/calendar" /></MenuItem>
            <MenuItem icon={<FaPrescriptionBottleAlt />}>Consultas<Link to="/prescription" /></MenuItem>
            <MenuItem icon={<FaNotesMedical />}>Pacientes<Link to="/patients" /></MenuItem>
            <MenuItem icon={<GoGraph />}>Metricas<Link to="/metrics" /></MenuItem>
          </Menu>
        </SidebarContent>

      <SidebarFooter style={{ textAlign: 'center' }}>
        <div
          className="sidebar-btn-wrapper"
          style={{
            padding: '2x 24px',
          }}
        >
          <button onClick={collaps} className="sidebar-btn">
            {collapsed ? <FaArrowCircleRight/> : <FaArrowCircleLeft/>}
          </button>
        </div>
      </SidebarFooter>
      
    </ProSidebar>
  );
}

export default SideMenu;